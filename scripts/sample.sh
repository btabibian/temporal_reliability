doc_size=(200)
source venv/bin/activate
seed_1=12121

seed_index=$(($seed_1+$1))
seed_2=$(($seed_2))
seed_3=$(($seed_1+$1))
for doc_index in ${doc_size[@]} ; do
  python -m src.experiment kernel -c configs/config_synthetic.cfg -o ./output/samples --verbose --print --lamb 0.0  --seed $seed_index --seed_2 $seed_2 --seed_3 $seed_3
  #--simulation ./test_output/2016-05-08_16-50-Test-s-1-d-50-item-1-T-15-.pickle
done
deactivate
rm ./outputs_kernel_doc_2/*.model.pickle
rm ./outputs_kernel_doc_2/*.simul.pickle
