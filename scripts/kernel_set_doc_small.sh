source venv/bin/activate
seed_1=12121

input_path=./output/merged_samples/
python -m src.experiment kernel -c configs/config_synthetic_small.cfg -o ./output/sample_test --verbose --print --lamb 0.5

deactivate
rm ./outputs_kernel_doc_2/*.model.pickle
rm ./outputs_kernel_doc_2/*.simul.pickle
