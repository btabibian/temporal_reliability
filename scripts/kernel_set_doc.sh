doc_size=(60 200 400 600 800 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000)
source venv/bin/activate
seed_1=12121

input_path=./output/merged_samples/
for doc_index in ${doc_size[@]} ; do
  python -m src.experiment kernel -c configs/config_synthetic.cfg -o ./output/reliability --verbose --print --lamb 0.5 --simulation "$input_path/syn_merged_all-d-"$doc_index
done
deactivate
rm ./outputs_kernel_doc_2/*.model.pickle
rm ./outputs_kernel_doc_2/*.simul.pickle
