from __future__ import division
import cvxpy as cvx
import numpy as np
from scipy.stats import norm
from scipy import sparse
import math
import logging
import time

def createDataSet_raw(events,docs_count,kernels_mean,kernels_length,source_mean,source_length):
  computeGrid_doc = lambda t: norm.pdf(np.reshape(t,(t.shape[0],1)),loc = kernels_mean,scale = kernels_length)
  computeGrid_doc_cdf = lambda t: norm.cdf(np.reshape(t,(t.shape[0],1)),loc = kernels_mean,scale = kernels_length)

  computeGrid_source = lambda t: norm.pdf(np.reshape(t,(t.shape[0],1)),loc = source_mean,scale = source_length)
  computeGrid_source_cdf = lambda t: norm.cdf(np.reshape(t,(t.shape[0],1)),loc = source_mean,scale = source_length)

  res = ((events['sites'],computeGrid_source(events['times'][:,1]),
        computeGrid_source_cdf(events['times'][:,1])-computeGrid_source_cdf(events['times'][:,0]),events['nodes'][:,0],
        computeGrid_doc(events['times'][:,1]),
        computeGrid_doc_cdf(events['times'][:,1])-computeGrid_doc_cdf(events['times'][:,0])),
       (events['sites_survived'],computeGrid_source(events['times_survived'][:,1]),
       computeGrid_source_cdf(events['times_survived'][:,1])-computeGrid_source_cdf(events['times_survived'][:,0]),events['nodes_survived'][:,0],
       computeGrid_doc(events['times_survived'][:,1]),
       computeGrid_doc_cdf(events['times_survived'][:,1])-computeGrid_doc_cdf(events['times_survived'][:,0])))
  return res

def _inferenceKernel(items,sites_count,documents_count,pi,beta_temp,alpha,lda=None,items_count=1,replications=1,iterations=10,lamb=1.0,fix_alpha=None,fix_beta=None):
  none_survived, survived = items
  sites_none_survived,t_site_none_srvived,t_diff_site_none_survived,d_none_survived,t_doc_none_survived,t_diff_doc_none_survived = none_survived

  if len(t_diff_doc_none_survived.shape)==1:
    t_diff_doc_none_survived = t_diff_doc_none_survived[:,np.newaxis]
  t_diff_doc_none_survived = t_diff_doc_none_survived+1e-7

  sites_none_survived = sites_none_survived.astype('int')
  sites_survived,t_site_srvived,t_diff_site_survived,d_survived,t_doc_survived,t_diff_doc_survived = survived
  if len(t_diff_doc_survived.shape)==1:
    t_diff_doc_survived = t_diff_doc_survived[:,np.newaxis]
  t_diff_doc_survived = t_diff_doc_survived+1e-7

  sites_survived = sites_survived.astype('int')

  d_non_surv = sparse.csr_matrix((np.ones(d_none_survived.shape[0]),(np.arange(d_none_survived.shape[0]),d_none_survived)),(d_none_survived.shape[0],documents_count))

  s_non_surv = sparse.csr_matrix((np.ones(sites_none_survived.shape[0]),(np.arange(sites_none_survived.shape[0]),sites_none_survived.flatten())),(sites_none_survived.shape[0],sites_count))

  d_surv = sparse.csr_matrix((np.ones(d_survived.shape[0]),(np.arange(d_survived.shape[0]),d_survived)),(d_survived.shape[0],documents_count))

  s_surv = sparse.csr_matrix((np.ones(sites_survived.shape[0]),(np.arange(sites_survived.shape[0]),sites_survived.flatten())),(sites_survived.shape[0],sites_count))

  beta = np.random.uniform(0,1,(documents_count,t_diff_doc_none_survived.shape[1]))
  alpha = np.random.uniform(0,1,(sites_count,t_diff_site_none_survived.shape[1]))
  alpha[:] = 1e-6
  if fix_alpha is not None:
    print('alpha set')
    alpha=fix_alpha
  if fix_beta is not None:
    beta=fix_beta

  print(beta.dtype,alpha.dtype)
  etta_iij = np.zeros((sites_none_survived.shape[0],t_diff_doc_none_survived.shape[1]))
  etta_iik = np.zeros((sites_none_survived.shape[0],t_diff_site_none_survived.shape[1]))
  print(etta_iij.dtype,etta_iik.dtype)
  print('setting EM algorithm')
  print(d_none_survived.shape)
  for iteration in range(iterations):
    #expectation
    print('iteration %d of %d' % (iteration,iterations))
    beta_prod = np.multiply(beta[d_none_survived.flatten(),:],(t_doc_none_survived))
    alpha_prod = np.multiply(alpha[sites_none_survived.flatten(),:],(t_site_none_srvived))
    norm_source = beta_prod.dot(np.ones(beta_prod.shape[1]))+alpha_prod.dot(np.ones(alpha_prod.shape[1]))
    #norm_source = norm_source.flatten()
    print('E step')
    for k in range(t_diff_site_none_survived.shape[1]):
      etta_iik[:,k] = alpha_prod[:,k]/norm_source
    for k in range(t_diff_doc_none_survived.shape[1]):
      etta_iij[:,k] = beta_prod[:,k]/norm_source
    print((etta_iik.sum(axis=1)+etta_iij.sum(axis=1)).min(),(etta_iik.sum(axis=1)+etta_iij.sum(axis=1)).max())
    etta_iij[etta_iij<1e-14] = 0
    print('M step')
    print(lamb)
    #MStep
    for k in range(beta.shape[1]):
      normalizer = (d_surv.T.dot(t_diff_doc_survived[:,k])+d_non_surv.T.dot(t_diff_doc_none_survived[:,k])+lamb)
      beta[:,k] = d_non_surv.T.dot(etta_iij[:,k])/(normalizer)
    if fix_alpha is not None:
      print('alpha set')
    else:
      for k in range(alpha.shape[1]):
        normalizer = s_surv.T.dot(t_diff_site_survived[:,k])+s_non_surv.T.dot(t_diff_site_none_survived[:,k])+1e-100
        alpha[:,k] = s_non_surv.T.dot(etta_iik[:,k])/(normalizer)
    print('average beta %f, alpha %f' % (np.median(beta),np.median(alpha)))


  return beta,alpha
def inferenceKernel(events,documents,sites, T, kernels_mean = None, kernels_length = None,
                    source_mean = None, source_length = None, fix_pi=None,fix_alpha=None,fix_beta=None,
                    fix_beta_temp=None,verbose=False,lamb=0.0,lda=None,iterations=300,replications=1,kernel_type='RBF', one_removal=False,approximation=True):
  docs_count = documents
  sites_count = sites#smodel['sites']
  # Construct the problem.
  pi = cvx.Variable(sites_count) if fix_pi is None else fix_pi
  if lda is None:
    alpha = cvx.Variable(sites_count) if fix_alpha is None else fix_alpha
  else:
    alpha = cvx.Variable(sites_count,lda.shape[1]) if fix_alpha is None else fix_alpha
  print(kernels_mean)
  beta_temp = cvx.Variable(docs_count,kernels_mean.shape[0]) if fix_beta_temp is None else fix_beta_temp
  loglikelihood = 0
  print(kernel_type,type(one_removal))
  if kernel_type=='RBF' and one_removal==False:
    items = createDataSet_raw(events,documents,kernels_mean,kernels_length,source_mean,source_length)
  else:
    raise Exception("Not supoorted!")

  beta,alpha = _inferenceKernel(items,sites_count,docs_count,pi,beta_temp,alpha,lda, replications=replications,iterations=iterations,lamb=lamb,fix_alpha=fix_alpha,fix_beta=fix_beta_temp)

  return 0,np.zeros(sites_count),alpha,beta

  return results,pi,alpha,beta_temp
