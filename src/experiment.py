'''
  Created on Mar 1, 2015

  @author: btabibian
  '''

import argparse
from . import model_experiment
import logging
import os
import datetime
import sys
import configparser
from configparser import ExtendedInterpolation

def experiment(args):
  config = configparser.ConfigParser(interpolation=ExtendedInterpolation())
  config.read(args.config)
  if args.real:
    data, path = model_experiment.experimentKernelReal(config,args.input,args.output,verbose=args.verbose,
                                                 processed_path=args.processed_path, store_processed=args.store_processed)
  else:
    data,model = model_experiment.experimentKernel(config,fix_pi=args.fix_pi,
                                            fix_alpha=args.fix_alpha, fix_beta_temp=args.fix_beta_kernels,
                                            output_path=args.output,
                                            simOnly=args.simOnly,lamb=args.lamb,
                                            model=args.model,simulation=args.simulation,verbose=args.verbose,
                                            seed=args.seed,seed_2=args.seed_2,seed_3=args.seed_3)

parserMain = argparse.ArgumentParser(description='Produces sample PSF/trajectory pairs')
parser = argparse.ArgumentParser(add_help=False)
parser.add_argument('-c','--config',type=str,required = True)
parser.add_argument('-o','--output', type=str, default='./outputs/',
                    help = 'Output directory.')
parser.add_argument('-m','--model',default=None ,type=str,help='path to a file with model')
parser.add_argument('--simulation',default=None,type=str,help='path to a file with simulation data')
parser.add_argument('--simOnly',default=False,action="store_true",help='Only simulate data.')
parser.add_argument('-r','--real',default = None,type=bool, help = 'real data')

parser.add_argument('--seed', type=int, default= None,
                    help = 'random seed.')
parser.add_argument('--seed_2', type=int, default= None,
                    help = 'random seed.')
parser.add_argument('--seed_3', type=int, default= None,
                    help = 'random seed.')
parser.add_argument('--iterations', type=int, default= 100,
                    help = 'number of iterations.')
parser.add_argument('--print', default=False,help='print on console',action='store_true')
parser.add_argument('--verbose',default=False,action="store_true",help='print optimization/debug messages.')
parser.add_argument('--logging',default='W',type=str,help = 'logging level, can be [W]arning,[E]rror,[D]ebug,[I]nfo,[C]ritical')
parser.add_argument('--logging_dir',default='./logs/',help='path for storing log files')
parser.add_argument('--timeit',default = 0, type=int,help='measure how long it takes to execute')
parser.add_argument('--processed_path',default=None,type=str,help='path to preprocessed data')
parser.add_argument('--store_processed',default=False, action='store_true',help='if provided stores result of processed data')

subparsers = parserMain.add_subparsers()
kernel_parser = subparsers.add_parser('kernel',parents=[parser],description='basic Grid space model')
kernel_parser.add_argument('--lamb', default=0.0, type=float,help = 'regularizer fo beta')
kernel_parser.add_argument('-p','--fix_pi', default=False,  action='store_true',
                    help = 'Fix pi.')
kernel_parser.add_argument('-k','--fix_beta_kernels', default=False, action='store_true',
                    help = 'Fix beta kernel.')
kernel_parser.add_argument('-a','--fix_alpha', default=False, action='store_true',help = 'Fix alpha.')
kernel_parser.set_defaults(func = experiment)

if __name__ == '__main__':
  args = parserMain.parse_args()
  level = logging.WARNING
  if args.logging == 'D':
    level = logging.DEBUG
  if args.logging == 'I':
    level = logging.INFO
  if args.logging == 'W':
    level = logging.WARNING
  if args.logging == 'E':
    level = logging.ERROR
  if args.logging == 'C':
    level = logging.CRITICAL
  config = configparser.ConfigParser(interpolation=ExtendedInterpolation())
  config.read(args.config)
  logging.basicConfig(filename=os.path.join(args.logging_dir,config['General']['Name']+"_"+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")+".log"),level=level)


  print('hheell')
  class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, log_level=logging.INFO):
      self.logger = logger
      self.log_level = log_level
      self.linebuf = ''

    def write(self, buf):
      for line in buf.rstrip().splitlines():
         self.logger.log(self.log_level, line.rstrip())

  root = logging.getLogger()
  sl = StreamToLogger(root, logging.INFO)
  if not args.print:
    sys.stdout = sl
  print('this is a test')
  if args.timeit > 0:
    import timeit
    t = timeit.Timer(lambda:args.func(args))
    logging.warning("Time it took to complete %f" % (t.timeit(args.timeit)/args.timeit))
  else:
    args.func(args)
