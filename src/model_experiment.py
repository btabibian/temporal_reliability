import numpy as np
from . import reliability, inference
import os
import datetime
import pickle
import logging
import math
from sklearn.preprocessing import normalize
import time

def generateModel(config,seed=123,seed_2=345):
  np.random.seed(seed_2)
  sites_count = int(config['General']['Libraries'])
  documents_count = int(config['General']['Projects'])
  pi_count = int(config['General']['Pi_count'])
  pi_drich = float(config['General']['Pi_drich'])

  one_removal = config.getboolean('Arrival', 'one_removal', fallback=False)
  mode = config.get('Arrival', 'mode', fallback="EXC")
  hawkes_kernel_type = config.get('Arrival', 'hawkes_kernel_type', fallback="RBF")
  # Kernel Positions
  kernels = np.linspace(0,float(config['General']['time'])-\
                          float(config['Survival']['kernels_gap_end']),
                          num=config['Survival']['kernels_count'])

  kernels_site = np.linspace(0,float(config['General']['time'])-\
                          float(config['Survival']['kernels_gap_end']),
                          num=config['Survival']['kernels_site_count'])

  kernels_arrival = np.linspace(0,float(config['General']['time'])-\
                          float(config['Arrival']['kernels_gap_end']),
                          num=config['Arrival']['kernels_count'])
  # Kernel Bandwidth
  length = np.linspace(float(config['Survival']['kernels_std']),
                       float(config['Survival']['kernels_std']),num=kernels.shape[0])

  length_site = np.linspace(float(config['Survival']['kernels_site_std']),
                       float(config['Survival']['kernels_site_std']),num=kernels_site.shape[0])

  length_arrival = np.linspace(float(config['Arrival']['kernels_std']),
                       float(config['Arrival']['kernels_std']),num=kernels_arrival.shape[0])
  # site distribution
  pi_ = np.random.uniform(size=sites_count)
  pi_ = pi_/pi_.sum()
  pi_[pi_<0.01] = 0.01
  pi_ = pi_/pi_.sum()

  # survival term for sites
  alpha_ = np.random.beta(float(config['Survival']['alpha_beta_a']),
                          float(config['Survival']['alpha_beta_b']),
                          (sites_count,int(config['Survival']['kernels_site_count'])))
  if 'alpha_max' in config['Survival']:
    alpha_ = (alpha_/(alpha_.max()))*float(config['Survival']['alpha_max'])
  # arrival excitation for sites
  gamma = np.random.uniform(float(config['Arrival']['gamma_uniform_min']),
                            float(config['Arrival']['gamma_uniform_max_ratio']),  (sites_count,1))
  np.random.seed(seed)
  np.random.seed(seed)
  phi = np.random.lognormal(float(config['Arrival']['phi_lognormal_mean'])
        ,sigma=float(config['Arrival']['phi_lognormal_std']),size=(documents_count,kernels_arrival.shape[0]))
  sparse_pat = np.random.multinomial(1, [1/kernels_arrival.shape[0]]*kernels_arrival.shape[0], size=documents_count).astype('bool')
  phi[np.logical_not(sparse_pat)] = 0.0
  w = np.zeros((documents_count,kernels.shape[0]))
  np.random.seed(seed)
  ratio = float(config['Survival']['beta_uniform_max_ratio'])
  if kernels.shape[0] == 1:
    w[:,0] =  np.random.uniform(float(config['Survival']['beta_uniform_min']),
                               float(config['Survival']['beta_uniform_max_ratio'])
                  ,(w.shape[0]))
  elif kernels.shape[0]==kernels_arrival.shape[0]:
    w[sparse_pat] = np.random.uniform(float(config['Survival']['beta_uniform_min']),
                  ratio*phi.sum(axis=1))
  else:
    raise Exception('Kernel shapes should be the same or survival kernels should have shape=1')
  #w = w.reshape(d,kernels.shape[0])
  pi = np.zeros((documents_count,sites_count))
  np.random.seed(seed)
  for d_ in np.arange(pi.shape[0]):
    pi[d_,np.random.choice(sites_count,pi_count,replace=False)]=np.random.dirichlet([pi_drich]*pi_count,1)
  return {'alpha':alpha_,'pi':pi,'w':w,
          'replications':int(config['General']['Replications']),

          'kernels':kernels,'length':length,'kernel_type':config['Survival']['kernel_type'],
          'kernels_arrival':kernels_arrival,'length_arrival':length_arrival,
          'kernels_site': kernels_site, 'length_site':length_site,
          'documents':documents_count,'sites':sites_count,

          'phi':phi,'gamma':gamma,
          'arrival_bandwidth':float(config['Arrival']['arrival_bandwidth']),
          'kernel_type_arrival':config['Arrival']['kernel_type'],'arrival_mode':mode,'one_removal':one_removal,'hawkes_kernel_type':hawkes_kernel_type,'T':float(config['General']['Time'])}

def mergeData(data_set):
  data_keys=list(data_set[0].keys())
  out = dict()
  for k in data_keys:
    out[k] = []
  for d in range(len(data_set)):
    for k in data_keys:
      out[k].append(data_set[d][k])
  for k in data_keys:
    out[k] = np.vstack(out[k])
  return out
def splitData(events,doc_count):
  def split(doc_index):
    selected = events['nodes'][:,0]==doc_index
    selected_survived = events['nodes_survived'][:,0]==doc_index
    return {"times":events['times'][selected,:],
            "nodes":events['nodes'][selected,:],
            "sites":events['sites'][selected,:],
            "times_survived":events['times_survived'][selected_survived,:],
            "nodes_survived":events['nodes_survived'][selected_survived,:],
            "sites_survived":events['sites_survived'][selected_survived,:]}
  return list(map(split,range(doc_count)))
def experimentKernel(config, fix_pi=False,fix_alpha=False,fix_beta_temp=False,output_path = "./output/", model = None, simulation = None, simOnly = False,verbose = False,seed=123,seed_2=987654,seed_3=321,lamb=0.0,predict=0):
  start_p_time = time.time()
  name = config['General']['Name']
  iterations = config.getint('Survival','iterations')
  T = float(config['General']['Time'])
  if simulation is None:
    model = generateModel(config,seed=seed,seed_2=seed_2)
    path = os.path.join(output_path,"%s-%s-s-%d-d-%d-item-%d-T-%d.model.pickle" %  (
                                                                           datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S"),
                                                                           name,model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],
                                                                           T))
    result = reliability.simulator(model,T,seed_3,split=False)
    path = os.path.join(output_path,"%s-%s-s-%d-d-%d-item-%d-T-%d.simul.pickle" % (
                                                                           datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S"),
                                                                           name,model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],T))
    fi = open(path,'wb')
    pickle.dump({'model':model,'data':splitData(result,model['documents']),'T':T},fi)
    fi.close()
    if simOnly:
      return
  else:
    fi = open(simulation,'rb')
    out = pickle.load(fi)
    result = out['data']
    model = out['model']
    if type(result) is list:
      result = mergeData(result)
    T = out['T']
  predict_set = None
  if predict > 0:
    result, predict_set = cross_validate.crossSample(result,predict,predict)

  #print(model['documents'],model['replications'])
  print(model['kernels'])
  results,pi,alpha,beta_temp = inference.inferenceKernel(result,model['documents'],
                                              model['sites'], T, model['kernels'],model['length'],
                                              fix_pi = model['pi'] if fix_pi else None,
                                              fix_alpha = model['alpha'] if fix_alpha else None,
                                              fix_beta_temp = model['w'] if fix_beta_temp else None,verbose = verbose,iterations=iterations,replications=model['replications'],lamb=lamb,kernel_type=model['kernel_type'],one_removal=model['one_removal'],source_mean=model['kernels_site'],source_length=model['length_site']
                                              )
  path = os.path.join(output_path,"%s-%s-s-%d-d-%d-item-%d-T-%d-.pickle" % (
                                                                           datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S"),
                                                                           name, model['sites'],
                                                                           model['documents'],
                                                                           model['replications'],T))
  fi = open(path,'wb')
  end_p_time = time.time()
  pickle.dump({'model':model,'negLoglike':results,'alpha':alpha, 'T':T,'data':result,'sample_count':result['nodes'].shape[0],
               'pi':pi,'beta_temp':beta_temp,'fix_alpha': fix_alpha, 'fix_beta_temp': fix_beta_temp, 'fix_pi': fix_pi,'processed_time':end_p_time-start_p_time,'predict':predict_set},fi)
  fi.close()
  fi = open(path,'rb')
  data = pickle.load(fi)
  return data,path

def experimentKernelReal(config,path,output_path = "./output/",
                         verbose=True,processed_path=None,store_processed=False,iterations=300):
  lda_topic_mean = None
  lda_titles = None
  lda = None
  documents = config.getint('General','limit_document',fallback=None)
  documents = None if documents == -1 else documents
  min_count = config.getint('General','min_count')
  min_items = config.getint('General','min_items')
  min_sites = config.getint('General','min_sites')

  kernels = config.getint('Survival','kernels_count')
  length = config.getfloat('Survival','kernels_std')
  kernel_type = config.get('Survival','kernel_type')
  one_removal =  config.getboolean('Arrival','one_removal')
  lamb =  config.getfloat('Survival','lamb')
  predict =  config.getfloat('Survival','predict',fallback=0.0)
  predict_doc = config.getfloat('Survival','predict_doc',fallback=0.0)
  lda_path =  config.get('Survival','topics_path',fallback=None)
  name = config.get('Survival','name')
  iterations = config.getint('Survival','iterations',fallback=iterations)
  termination = config.getfloat('General','termination',fallback=None)

  if lda_path is not None:
    lda_res = pickle.load(open(lda_path,'rb'))
    lda,lda_titles = lda_res[0],lda_res[1]
    #from sklearn.preprocessing import normalize
    #lda_topic_mean = np.array(lda.mean(axis=0)).flatten()
    #lda_selected_topics = lda[:,lda_topic_mean>lda_topic_threshold]
    #lda_selected_docs_index = np.array((lda_selected_topics.sum(axis=1)>lda_document_threshold)).ravel()
    #lda_selected = lda_selected_topics[lda_selected_docs_index,:]
    #print("number of documents in selected lda %d, number of topics %d" % (lda_selected.shape[0],lda_selected.shape[1] ))
    lda = normalize(lda, norm='l1', axis=1)
    #lda_titles = lda_titles[lda_selected_docs_index]
  else:
    lda=None

  if processed_path:
    data_, sites,titles,start,end,lda_out = pickle.load(open(processed_path,'rb'))
  else:
    from wikidatasets import wikidataRead
    data_,sites,titles,start,end,lda_out = wikidataRead.loadFile(path,documents,min_count,min_items,min_sites=min_sites,lda_matrix=lda,lda_titles=lda_titles)

  if lda_path is not None:
    lda = lda[lda_out,:]
    lda_titles = lda_titles[lda_out]
  #print("Max sites %d" % len(sites))
  #print(lda.shape, "lda shape")
  if predict > 0:
    data, predict_set = cross_validate.crossSample(data_,predict,predict)
  elif predict_doc > 0:
    data, predict_set = cross_validate.crossSampleDoc(data_,predict_doc)
    predict = predict_doc
  else:
    data = data_
  if type(kernels) is int:
    size = kernels
  else:
    size = len(kernels)
  max_time = max(data['times'].max(),data['times_survived'].max())
  max_document = max(data['nodes'][:,0].max(),data['nodes_survived'][:,0].max())
  max_item = max(data['nodes'][:,1].max(),data['nodes_survived'][:,1].max())
  max_sites = len(sites)-1
  print(max_document, "document")
  if store_processed and processed_path is None:
    print("store processed")
    path_out = os.path.join(output_path,"%s-%s-s-%d-d-%d-T-%d.post.pickle" % (
                                                                           datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S"),
                                                                           name,len(sites),
                                                                           int(max_document)+1,max_time,
                                                                           datetime.datetime.now()))
    fi = open(path_out,'wb')
    pickle.dump((data_,sites,titles,start,end,lda_out),fi)
    fi.close()
  length = np.linspace(length,length,num=size)

  if type(kernels) is int:
    kernels = np.linspace(0,max_time,num=kernels)
  else:
    kernels = np.array(kernels)
  logging.info('starting inference')
  results,pi,alpha,beta_temp = inference.inferenceKernel(data,
                                                       int(max_document)+1,
                                                       int(max_sites)+1,termination,kernels,length,
                                                         verbose = verbose,lamb=lamb,lda = lda,iterations=iterations,one_removal=one_removal,kernel_type=kernel_type)
  path_out = os.path.join(output_path,"%s-%s-s-%d-d-%d-T-%d.pickle" % (
                                                                        datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S"), name,len(sites),
                                                                        int(max_document)+1,max_time))
  fi = open(path_out,'wb')
  export_dict = {'model':{'kernels':kernels,'length':length},'data':data,'negLoglike':results,'alpha':alpha,
               'T':max_time,'pi':pi,'beta_temp':beta_temp,'sites':sites,'titles':titles,'start':start,'lamb':lamb}
  if predict>0:
    export_dict['predict'] = predict_set
  if lda_path is not None:
    export_dict['lda_matrix'] = lda
    export_dict['lda_titles'] = np.asarray(lda_titles)
    #export_dict['lda_selected_topics'] = lda_topic_mean>lda_topic_threshold
  pickle.dump(export_dict,fi)
  fi.close()
  fi = open(path_out,'rb')
  data = pickle.load(fi)
  return data,path
